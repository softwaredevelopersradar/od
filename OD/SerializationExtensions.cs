﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace llcss
{
    public static class SerializationExtensions
    {
        public static byte[] GetBytes(this IBinarySerializable self)
        {
            var buffer = new byte[self.StructureBinarySize];
            self.GetBytes(buffer, 0);
            return buffer;
        }

        public static bool TryGetBytes(this IBinarySerializable self, out byte[] result)
        {
            try
            {
                result = self.GetBytes();
                return true;
            }
            catch (Exception)
            {
                result = null;
                return false;
            }
        }

        public static byte[] GetBytes(DateTime self, byte[] buffer, int shiftIndex)
        {
            GetBytes(self.Year, 2, buffer, shiftIndex + 0);
            buffer[shiftIndex + 2] = (byte)self.Month;
            buffer[shiftIndex + 3] = (byte)self.Day;
            buffer[shiftIndex + 4] = (byte)self.Hour;
            buffer[shiftIndex + 5] = (byte)self.Minute;
            buffer[shiftIndex + 6] = (byte)self.Second;
            GetBytes(self.Millisecond, 2, buffer, shiftIndex + 7);

            return buffer;
        }

        public static DateTime DecodeDateTime(byte[] buffer, int shiftIndex)
        {
            var year = Decode16(buffer, shiftIndex);
            var month = buffer[shiftIndex + 2];
            var day = buffer[shiftIndex + 3];
            var hour = buffer[shiftIndex + 4];
            var minute = buffer[shiftIndex + 5];
            var second = buffer[shiftIndex + 6];
            var millisecond = Decode16(buffer, shiftIndex + 7);

            return new DateTime(year, month, day, hour, minute, second, millisecond);
        }

        public static byte[] GetBytes(TimeSpan self, byte[] buffer, int shiftIndex)
        {
            buffer[shiftIndex + 0] = (byte)self.Days;
            buffer[shiftIndex + 1] = (byte)self.Hours;
            buffer[shiftIndex + 2] = (byte)self.Minutes;
            buffer[shiftIndex + 3] = (byte)self.Seconds;
            GetBytes(self.Milliseconds, 2, buffer, shiftIndex + 4);

            return buffer;
        }

        public static bool IsValidDateTime(byte[] buffer, int shiftIndex)
        {
            return buffer.Length - shiftIndex >= 9;
        }

        public static bool IsValidTimeSpan(byte[] buffer, int shiftIndex)
        {
            return buffer.Length - shiftIndex >= 6;
        }

        public static TimeSpan DecodeTimeSpan(byte[] buffer, int shiftIndex)
        {
            var day = buffer[shiftIndex + 0];
            var hour = buffer[shiftIndex + 1];
            var minute = buffer[shiftIndex + 2];
            var second = buffer[shiftIndex + 3];
            var millisecond = Decode16(buffer, shiftIndex + 4);

            return new TimeSpan(day, hour, minute, second, millisecond);
        }

        public static int BinarySize(object value)
        {
            return System.Runtime.InteropServices.Marshal.SizeOf(value);
        }

        public static int BinarySize(DateTime value)
        {
            return 9;
        }

        public static int BinarySize(TimeSpan value)
        {
            return 6;
        }

        public static int BinarySize<T>(T[] value)
        {
            return value.Sum(v => BinarySize(v));
        }

        public static int BinarySize<T>(List<T> value)
        {
            return 4 + value.Sum(v => BinarySize(v));
        }

        public static int BinarySize(IBinarySerializable value)
        {
            return value.StructureBinarySize;
        }

        public static void Initialize<T>(this List<T> list, int count)
        {
            if (list == null)
            {
                throw new ArgumentNullException("list");
            }

            if (list.Count != 0)
            {
                throw new InvalidOperationException("list already initialized");
            }
            list.AddRange(Enumerable.Repeat(default(T), count));
        }

        public static int BinarySize(string value)
        {
            if (value == null)
                return 4;
            return 4 + 2 * value.Length;
        }

        public static void GetBytes(long value, int byteCount, byte[] buffer, int shiftIndex)
        {
            var shift = 0;
            for (int i = 0; i < byteCount; ++i)
            {
                buffer[shiftIndex + i] = (byte)(value >> shift);
                shift += 8;
            }
        }

        public static void GetBytes(int value, int byteCount, byte[] buffer, int shiftIndex)
        {
            var shift = 0;
            for (int i = 0; i < byteCount; ++i)
            {
                buffer[shiftIndex + i] = (byte)(value >> shift);
                shift += 8;
            }
        }

        public static void GetBytes(short value, int byteCount, byte[] buffer, int shiftIndex)
        {
            var shift = 0;
            for (int i = 0; i < byteCount; ++i)
            {
                buffer[shiftIndex + i] = (byte)(value >> shift);
                shift += 8;
            }
        }

        public static void GetBytes(ulong value, int byteCount, byte[] buffer, int shiftIndex)
        {
            var shift = 0;
            for (int i = 0; i < byteCount; ++i)
            {
                buffer[shiftIndex + i] = (byte)(value >> shift);
                shift += 8;
            }
        }

        public static void GetBytes(uint value, int byteCount, byte[] buffer, int shiftIndex)
        {
            var shift = 0;
            for (int i = 0; i < byteCount; ++i)
            {
                buffer[shiftIndex + i] = (byte)(value >> shift);
                shift += 8;
            }
        }

        public static void GetBytes(ushort value, int byteCount, byte[] buffer, int shiftIndex)
        {
            var shift = 0;
            for (int i = 0; i < byteCount; ++i)
            {
                buffer[shiftIndex + i] = (byte)(value >> shift);
                shift += 8;
            }
        }

        public static short Decode16(byte[] buffer, int shiftIndex)
        {
            return (short)((buffer[shiftIndex + 1] << 8) + buffer[shiftIndex]);
        }

        public static int Decode24(byte[] buffer, int shiftIndex)
        {
            return (buffer[shiftIndex + 2] << 16) + (buffer[shiftIndex + 1] << 8) + buffer[shiftIndex];
        }

        public static int Decode32(byte[] buffer, int shiftIndex)
        {
            return (buffer[shiftIndex + 3] << 24) + (buffer[shiftIndex + 2] << 16) + (buffer[shiftIndex + 1] << 8) + buffer[shiftIndex];
        }

        public static long Decode40(byte[] buffer, int shiftIndex)
        {
            return Decode(buffer, shiftIndex, 5);
        }

        public static long Decode48(byte[] buffer, int shiftIndex)
        {
            return Decode(buffer, shiftIndex, 6);
        }

        public static long Decode56(byte[] buffer, int shiftIndex)
        {
            return Decode(buffer, shiftIndex, 7);
        }

        public static long Decode64(byte[] buffer, int shiftIndex)
        {
            return Decode(buffer, shiftIndex, 8);
        }

        public static ushort DecodeUnsigned16(byte[] buffer, int shiftIndex)
        {
            return (ushort)((buffer[shiftIndex + 1] << 8) + buffer[shiftIndex]);
        }

        public static uint DecodeUnsigned24(byte[] buffer, int shiftIndex)
        {
            return (uint)(buffer[shiftIndex + 2] << 16) + (uint)(buffer[shiftIndex + 1] << 8) + buffer[shiftIndex];
        }

        public static uint DecodeUnsigned32(byte[] buffer, int shiftIndex)
        {
            return (uint)(buffer[shiftIndex + 3] << 24) + (uint)(buffer[shiftIndex + 2] << 16) + (uint)(buffer[shiftIndex + 1] << 8) + buffer[shiftIndex];
        }

        public static ulong DecodeUnsigned40(byte[] buffer, int shiftIndex)
        {
            return DecodeUnsigned(buffer, shiftIndex, 5);
        }

        public static ulong DecodeUnsigned48(byte[] buffer, int shiftIndex)
        {
            return DecodeUnsigned(buffer, shiftIndex, 6);
        }

        public static ulong DecodeUnsigned56(byte[] buffer, int shiftIndex)
        {
            return DecodeUnsigned(buffer, shiftIndex, 7);
        }

        public static ulong DecodeUnsigned64(byte[] buffer, int shiftIndex)
        {
            return DecodeUnsigned(buffer, shiftIndex, 8);
        }

        public static long Decode(byte[] buffer, int shiftIndex, int byteCount)
        {
            var shift = 0;
            long value = 0;

            for (int i = 0; i < byteCount; ++i)
            {
                value += (long)buffer[shiftIndex + i] << shift;
                shift += 8;
            }
            return value;
        }

        public static ulong DecodeUnsigned(byte[] buffer, int shiftIndex, int byteCount)
        {
            var shift = 0;
            ulong value = 0;

            for (int i = 0; i < byteCount; ++i)
            {
                value += (ulong)buffer[shiftIndex + i] << shift;
                shift += 8;
            }
            return value;
        }
    }

    public interface IBinarySerializable
    {
        void Decode(byte[] buffer, int startIndex);
        void GetBytes(byte[] buffer, int startIndex);
        int StructureBinarySize { get; }
    }
}