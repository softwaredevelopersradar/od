﻿using System;
using System.Windows;

namespace OD
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        LocalSettings localSettings;
        Yaml yaml = new Yaml();
        Server ODServer = new Server();

        public MainWindow()
        {
            InitializeComponent();

            //localSettings = yaml.Load();
            localSettings = yaml.YamlLoad<LocalSettings>("LocalSettings.yaml");

            MyPG.SelectedObject = localSettings;

            InitAPDEvents();
        }

        public string ToTextString(string message)
        {
            DateTime dT = DateTime.Now;
            return $"{dT.Hour.ToString("D2")}:{dT.Minute.ToString("D2")}:{dT.Second.ToString("D2")}:{dT.Millisecond.ToString("D3")} " + message + "\r\n";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            // textBlock.Text = textBlock.Text + ToTextString("Client Connected");
            //yaml.Save(localSettings);
        }

        private void ODConnection_Click(object sender, RoutedEventArgs e)
        {

        }

        private void OPConnection_Click(object sender, RoutedEventArgs e)
        {

        }

        private void DBConnection_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //yaml.Save((LocalSettings)MyPG.SelectedObject);
            yaml.YamlSave<LocalSettings>((LocalSettings)MyPG.SelectedObject, "LocalSettings.yaml");
        }


        bool flagPU = false;
        private void APDConnection_Click(object sender, RoutedEventArgs e)
        {
            flagPU = !flagPU;
            if (flagPU) APDControlConnection.ShowConnect();
            else APDControlConnection.ShowDisconnect();
        }
    }
}
