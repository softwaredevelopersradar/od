﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace OD
{
    public class ComboBoxWidthConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var cb = values[0] as ComboBox;
            var items = cb.Items;

            List<double> ld = new List<double>();
            foreach (ComboBoxItem item in items)
            {
                var tmp = GetTextSize(item, item.Content.ToString());
                ld.Add(tmp.Width);
            }

            return ld.Max() + 30; //30 пикселей на стрелочку и отступы
        }

        private static Size GetTextSize(ComboBoxItem item, string text)
        {
            FormattedText ft = new FormattedText(text, CultureInfo.CurrentUICulture, item.FlowDirection,
                                                 new Typeface(item.FontFamily, item.FontStyle,
                                                              item.FontWeight, item.FontStretch),
                                                 item.FontSize, item.Foreground
                );

            return new Size(ft.Width, ft.Height);
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException("ConvertBack in MultiBoolsToBoolConverter nicht implementiert");
        }
    }
}
