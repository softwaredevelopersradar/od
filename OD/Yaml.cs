﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;

namespace OD
{
    public class Yaml
    {
        public LocalSettings Load()
        {
            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader("LocalSettings.yaml", System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            var deserializer = new DeserializerBuilder().Build();

            var localSettings = new LocalSettings();
            try
            {
                localSettings = deserializer.Deserialize<LocalSettings>(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            if (localSettings == null) localSettings = new LocalSettings();
            return localSettings;
        }

        public void Save(LocalSettings localSettings)
        {
            var serializer = new SerializerBuilder().EmitDefaults().Build();
            var yaml = serializer.Serialize(localSettings);

            using (StreamWriter sw = new StreamWriter("LocalSettings.yaml", false, System.Text.Encoding.Default))
            {
                sw.WriteLine(yaml);
                sw.Close();
            }
        }

        public T YamlLoad<T>(string NameDotYaml) where T : new()
        {
            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader(NameDotYaml, System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            var deserializer = new DeserializerBuilder().Build();

            var t = new T();
            try
            {
                t = deserializer.Deserialize<T>(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            if (t == null)
            {
                t = new T();
                YamlSave(t, NameDotYaml);
            }
            return t;
        }

        public void YamlSave<T>(T t, string NameDotYaml) where T : new()
        {
            try
            {
                var serializer = new SerializerBuilder().EmitDefaults().Build();
                var yaml = serializer.Serialize(t);

                using (StreamWriter sw = new StreamWriter(NameDotYaml, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

    }
}
