﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows.Data;

namespace OD
{
    [CategoryOrder("Установка параметров типов связи", 1)]
    [CategoryOrder("Интервал опроса, c", 2)]
    public class LocalSettings : INotifyPropertyChanged
    {
        public LocalSettings()
        {
            Role = OD_Role.Client;
            ConnectionType = OD_ConnectionType.Tainet;

            PU_CType = PU_ConnectionType.APD;

            Tainet = new COM();
            MobileNetwork = new IPnPort();
            RRS = new IPnPort();
            FiberOptics = new IPnPort();
            Database = new IPnPort();
            APD = new COM();

            //Tainet.PropertyChanged += sPropertyChanged;
            //MobileNetwork.PropertyChanged += sPropertyChanged;
            //RRS.PropertyChanged += sPropertyChanged;
            //FiberOptics.PropertyChanged += sPropertyChanged;
        }

        //private void sPropertyChanged(object sender, PropertyChangedEventArgs e)
        //{
        //    PropertyChanged?.Invoke(sender, e);
        //}

        public string GetIP()
        {
            return "127.0.0.1";
        }

        [Category("ОД")]
        [PropertyOrder(1)]
        [DisplayName("Роль")]
        public OD_Role Role { get; set; }

        [Category("ОД")]
        [PropertyOrder(2)]
        [DisplayName("Тип связи")]
        public OD_ConnectionType ConnectionType { get; set; }

        [Category("ПУ")]
        [DisplayName("Тип связи")]
        public PU_ConnectionType PU_CType { get; set; }

        [Category("Установка параметров типов связи")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [PropertyOrder(1)]
        [DisplayName("Tainet")]
        public COM Tainet { get; set; }

        [Category("Установка параметров типов связи")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [PropertyOrder(2)]
        [DisplayName("3G/4G")]
        public IPnPort MobileNetwork { get; set; }

        [Category("Установка параметров типов связи")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [PropertyOrder(3)]
        [DisplayName("РРС")]
        public IPnPort RRS { get; set; }

        [Category("Установка параметров типов связи")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [PropertyOrder(4)]
        [DisplayName("Оптоволокно")]
        public IPnPort FiberOptics { get; set; }

        [Category("Установка параметров типов связи")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [PropertyOrder(5)]
        [DisplayName("АПД")]
        public COM APD { get; set; }

        [Category("Интервал опроса, c")]
        [DisplayName("Состояния АСП")]
        [PropertyOrder(1)]
        public int StatusPollingInterval { get; set; }

        [Category("Интервал опроса, c")]
        [DisplayName("ИРИ ЦР")]
        [PropertyOrder(2)]
        public int TargettingPollingInterval { get; set; }

        [Category("Интервал опроса, c")]
        [DisplayName("ИРИ РП")]
        [PropertyOrder(3)]
        public int SuppressionPollingInterval { get; set; }

        [Category("База данных")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [DisplayName("Адрес")]
        public IPnPort Database { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class IPnPort : INotifyPropertyChanged
    {
        private string _IP;
        private int _Port;

        [NotifyParentProperty(true)]
        public string IP
        {
            get { return _IP; }
            set
            {
                if (_IP == value) return;
                _IP = value;
                OnPropertyChanged("IP");
            }
        }

        [NotifyParentProperty(true)]
        public int Port
        {
            get { return _Port; }
            set
            {
                if (_Port == value) return;
                _Port = value;
                OnPropertyChanged("Port");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public IPnPort()
        {
            IP = "127.0.0.1";
            Port = 10000;
        }

        public override string ToString()
        {
            return IP + " : " + Port;
        }
    }

    public class COM : INotifyPropertyChanged
    {
        private string _COMPort;
        private SpeedRate _Rate;

        [NotifyParentProperty(true)]
        public string COMPort
        {
            get { return _COMPort; }
            set
            {
                if (_COMPort == value) return;
                _COMPort = value;
                OnPropertyChanged("COMPort");
            }
        }

        [NotifyParentProperty(true)]
        public SpeedRate Rate
        {
            get { return _Rate; }
            set
            {
                if (_Rate == value) return;
                _Rate = value;
                OnPropertyChanged("Rate");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public COM()
        {
            _COMPort = "COM 1";
            _Rate = SpeedRate.Speed_9600;
        }

        public override string ToString()
        {
            return _COMPort + " / " + (int)_Rate;
        }
    }

    public enum SpeedRate
    {
        [Description("2400")]
        Speed_2400 = 2400,
        [Description("4800")]
        Speed_4800 = 4800,
        [Description("9600")]
        Speed_9600 = 9600,
        [Description("19200")]
        Speed_19200 = 19200,
        [Description("38400")]
        Speed_38400 = 38400,
        [Description("57600")]
        Speed_57600 = 57600,
        [Description("115200")]
        Speed_115200 = 115200
    }

    public enum OD_Role
    {
        [Description("Клиент")]
        Client,
        [Description("Сервер")]
        Server
    }

    public enum OD_ConnectionType
    {
        [Description("Tainet")]
        Tainet,
        [Description("МобильнаяСвязь")]
        MobileConnection,
        [Description("РРС")]
        RRS,
        [Description("Оптоволокно")]
        FiberOptics,
    }

    public enum PU_ConnectionType
    {
        [Description("АПД")]
        APD
    }
}
